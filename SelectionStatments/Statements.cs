﻿using System;

namespace SelectionStatments
{
#pragma warning disable
    public static class Statements
    {
        public static void WriteLargestWithNestedIfElse(int first, int second, int third)
        {
            if (first >= second && first >= third)
            {
                Console.WriteLine($"Number {first} is the largest");
                return;
            }

            if (second >= first && second >= third)
            {
                Console.WriteLine($"Number {second} is the largest");
                return;
            }

            Console.WriteLine($"Number {third} is the largest");
        }

        public static void WriteLargestWithIfElseAndTernaryOperator(int first, int second, int third)
        {
            if (first >= second && first >= third)
            {
                Console.WriteLine($"Number {first} is the largest");
            }
            else
            {
                Console.WriteLine(second >= first && second >= third
                    ? $"Number {second} is the largest"
                    : $"Number {third} is the largest");
            }
        }

        public static void WriteLargestWithIfElseAndConditionLogicalOperators(int first, int second, int third)
        {
            if (first >= second && first >= third)
            {
                Console.WriteLine($"Number {first} is the largest");
            }
            else if (second >= first && second >= third)
            {
                Console.WriteLine($"Number {second} is the largest");
            }
            else
            {
                Console.WriteLine($"Number {third} is the largest");
            }
        }

        public static void HowOldAreYouReactionWithCascadedIfElse(int userAge)
        {
            if (userAge >= 65)
            {
                Console.WriteLine("Enjoy your retirement!");
            }
            else if (userAge >= 21)
            {
                Console.WriteLine("Fancy an alcoholic beverage?");
            }
            else if (userAge >= 18)
            {
                Console.WriteLine("You're old enough to drive.");
            }
            else
            {
                Console.WriteLine("You are too young to drive, drink, or retire.");
            }
        }

        public static void WriteInformationAboutDailyDownloadsWithCascadedIfElse(int countOfDailyDownloads)
        {
            if (countOfDailyDownloads <= 0)
            {
                Console.WriteLine("No downloads.");
            }
            else if (countOfDailyDownloads < 100)
            {
                Console.WriteLine("Daily downloads: 1-100.");
            }
            else if (countOfDailyDownloads < 1000)
            {
                Console.WriteLine("Daily downloads: 100-1,000.");
            }
            else if (countOfDailyDownloads < 10000)
            {
                Console.WriteLine("Daily downloads: 1,000-10,000.");
            }
            else if (countOfDailyDownloads < 100000)
            {
                Console.WriteLine("Daily downloads: 10,000-100,000.");
            }
            else
            {
                Console.WriteLine("Daily downloads: 100,000+.");
            }
        }

        public static void WriteTheInformationAboutDayWithIfElse(DayOfWeek dayOfWeek)
        {
            if (dayOfWeek == DayOfWeek.Saturday || dayOfWeek == DayOfWeek.Sunday)
            {
                Console.WriteLine("The weekend.");
            }
            else if (dayOfWeek == DayOfWeek.Monday)
            {
                Console.WriteLine("The first day of the work week.");
            }
            else if (dayOfWeek == DayOfWeek.Friday)
            {
                Console.WriteLine("The last day of the work week.");
            }
            else
            {
                Console.WriteLine("The middle of the work week.");
            }
        }

        public static void WriteTheInformationAboutDayWithSwitchStatement(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    Console.WriteLine("The weekend.");
                    break;
                case DayOfWeek.Monday:
                    Console.WriteLine("The first day of the work week.");
                    break;
                case DayOfWeek.Friday:
                    Console.WriteLine("The last day of the work week.");
                    break;
                default:
                    Console.WriteLine("The middle of the work week.");
                    break;
            }
        }

        public static string GetTypeOfIntegerWithCascadedIfElse(object arg)
        {
            if (arg is sbyte)
            {
                return $"{arg} is sbyte.";
            }

            if (arg is byte)
            {
                return $"{arg} is byte.";
            }

            if (arg is short)
            {
                return $"{arg} is short.";
            }

            if (arg is int)
            {
                return $"{arg} is int.";
            }

            if (arg is long)
            {
                return $"{arg} is long.";
            }

            if (arg is ushort)
            {
                return $"{arg} is ushort.";
            }

            if (arg is uint)
            {
                return $"{arg} is uint.";
            }

            if (arg is ulong)
            {
                return $"{arg} is ulong.";
            }

            return $"{arg} is not integer.";
        }

        public static string GetTypeOfIntegerWithSwitchStatement(object arg)
        {
            switch (arg)
            {
                case sbyte _:
                    return $"{arg} is sbyte.";
                case byte _:
                    return $"{arg} is byte.";
                case short _:
                    return $"{arg} is short.";
                case int _:
                    return $"{arg} is int.";
                case long _:
                    return $"{arg} is long.";
                case ushort _:
                    return $"{arg} is ushort.";
                case uint _:
                    return $"{arg} is uint.";
                case ulong _:
                    return $"{arg} is ulong.";
                default:
                    return $"{arg} is not integer.";
            }
        }

        public static string GetTypeOfIntegerWithSwitchExpression(object arg)
        {
            return arg switch
            {
                sbyte _ => $"{arg} is sbyte.",
                byte _ => $"{arg} is byte.",
                short _ => $"{arg} is short.",
                int _ => $"{arg} is int.",
                long _ => $"{arg} is long.",
                ushort _ => $"{arg} is ushort.",
                uint _ => $"{arg} is uint.",
                ulong _ => $"{arg} is ulong.",
                _ => $"{arg} is not integer."
            };
        }

        public static void WriteTheInformationAboutSeasonsWithSwitchStatement(Month month)
        {
            switch (month)
            {
                case Month.December:
                case Month.January:
                case Month.February:
                    Console.WriteLine("It's winter now.");
                    break;
                case Month.March:
                case Month.April:
                case Month.May:
                    Console.WriteLine("It's spring now.");
                    break;
                case Month.June:
                case Month.July:
                case Month.August:
                    Console.WriteLine("It's summer now.");
                    break;
                case Month.September:
                case Month.October:
                case Month.November:
                    Console.WriteLine("It's autumn now.");
                    break;
                default:
                    Console.WriteLine("Sorry, the month was entered incorrectly.");
                    break;
            }
        }

        public static byte GetLengthWithCascadedIfElse(int number)
        {
            if (number == int.MinValue)
            {
                return 10;
            }

            if (number < 0)
            {
                number = -number;
            }

            if (number < 10)
            {
                return 1;
            }

            if (number < 100)
            {
                return 2;
            }

            if (number < 1000)
            {
                return 3;
            }

            if (number < 10000)
            {
                return 4;
            }

            if (number < 100000)
            {
                return 5;
            }

            if (number < 1000000)
            {
                return 6;
            }

            if (number < 10000000)
            {
                return 7;
            }

            if (number < 100000000)
            {
                return 8;
            }

            if (number < 1000000000)
            {
                return 9;
            }

            return 10;
        }

        public static byte GetLengthWithSwitchExpression(int number)
        {
            if (number == int.MinValue)
            {
                return 10;
            }

            if (number < 0)
            {
                number = -number;
            }

            return number switch
            {
                _ when number < 10 => 1,
                _ when number < 100 => 2,
                _ when number < 1000 => 3,
                _ when number < 10000 => 4,
                _ when number < 100000 => 5,
                _ when number < 1000000 => 6,
                _ when number < 10000000 => 7,
                _ when number < 100000000 => 8,
                _ when number < 1000000000 => 9,
                _ => 10
            };
        }

        public static Month? GetMonthWithCascadedIfElse(int month)
        {
            if (month == 1)
            {
                return Month.January;
            }

            if (month == 2)
            {
                return Month.February;
            }

            if (month == 3)
            {
                return Month.March;
            }

            if (month == 4)
            {
                return Month.April;
            }

            if (month == 5)
            {
                return Month.May;
            }

            if (month == 6)
            {
                return Month.June;
            }

            if (month == 7)
            {
                return Month.July;
            }

            if (month == 8)
            {
                return Month.August;
            }

            if (month == 9)
            {
                return Month.September;
            }

            if (month == 10)
            {
                return Month.October;
            }

            if (month == 11)
            {
                return Month.November;
            }

            if (month == 12)
            {
                return Month.December;
            }

            return null;
        }

        public static Month? GetMonthWithSwitchStatement(int month)
        {
            switch (month)
            {
                case 1:
                    return Month.January;
                case 2:
                    return Month.February;
                case 3:
                    return Month.March;
                case 4:
                    return Month.April;
                case 5:
                    return Month.May;
                case 6:
                    return Month.June;
                case 7:
                    return Month.July;
                case 8:
                    return Month.August;
                case 9:
                    return Month.September;
                case 10:
                    return Month.October;
                case 11:
                    return Month.November;
                case 12:
                    return Month.December;
                default:
                    return null;
            }
        }

        public static Month? GetMonthWithSwitchExpression(int month)
        {
            return month switch
            {
                1 => Month.January,
                2 => Month.February,
                3 => Month.March,
                4 => Month.April,
                5 => Month.May,
                6 => Month.June,
                7 => Month.July,
                8 => Month.August,
                9 => Month.September,
                10 => Month.October,
                11 => Month.November,
                12 => Month.December,
                _ => null
            };
        }
    }
}
